const data = {
    "product_types": [
        {
            "id": 0,
            "name": "Électroménager",
            "icon_url": ""
        },
        {
            "id": 1,
            "name": "Électronique",
            "icon_url": ""
        },
        {
            "id": 2,
            "name": "Mobilier",
            "icon_url": ""
        },
        {
            "id": 3,
            "name": "Décoration",
            "icon_url": ""
        },
        {
            "id": 4,
            "name": "Accessoires de Sport",
            "icon_url": ""
        },
        {
            "id": 5,
            "name": "Médical",
            "icon_url": ""
        }
    ],
    "products": {
        "Machine à laver": "https://cdn2.iconfinder.com/data/icons/home-sweet-home-4/320/Wash_machine-512.png",
        "Micro-ondes": null,
        "Déambulateur": "https://image.flaticon.com/icons/svg/1971/1971403.svg",
        "Sèche-linge": null,
        "Téléphone": null,
        "Tablette": null,
        "Ordinateur": null,
        "Canapé": null,
        "Table": null,
        "Armoire": null,
        "Lampe": null,
        "Cadre": null,
        "Vase": null,
        "Table de ping-pong": null,
        "Balle de gym": null,
        "Skateboard": null,
        "Lunettes": null,
        "Appareil auditif": null,
    },
    "productType": {
        "Machine à laver": 0,
        "Micro-ondes": 0,
        "Déambulateur": 5,
        "Sèche-linge": 0,
        "Téléphone": 1,
        "Tablette": 1,
        "Ordinateur": 1,
        "Canapé": 2,
        "Table": 2,
        "Armoire": 2,
        "Lampe": 3,
        "Cadre": 3,
        "Vase": 3,
        "Table de ping-pong": 4,
        "Balle de gym": 4,
        "Skateboard": 4,
        "Lunettes": 5,
        "Appareil auditif": 5,
    },
    "service_types": [
        {
            "id": 0,
            "name": "Réparateur"
        },
        {
            "id": 1,
            "name": "Achat de pièces détachées"
        },
        {
            "id": 2,
            "name": "Point de collecte"
        }
    ],
    "services": [
        {
            "name": "Lou Electroménager",
            "description": "Description de l'associaiton / entreprise ...",
            "logo": "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a3/Recycling_symbol2.svg/1024px-Recycling_symbol2.svg.png",
            "url": "https://www.lou-electromenager.fr/",
            "service_type_id": 0,
            "obj_type": [
                0
            ]
        },
        {
            "name": "RDS Pièces détachées",
            "description": "Description de l'associaiton / entreprise ...",
            "logo": "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a3/Recycling_symbol2.svg/1024px-Recycling_symbol2.svg.png",
            "url": "https://www.pieces-menager.fr/",
            "service_type_id": 1,
            "obj_type": [
                0
            ]
        },
        {
            "name": "Emmaus Vienne",
            "description": "Description de l'associaiton / entreprise ...",
            "logo": "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a3/Recycling_symbol2.svg/1024px-Recycling_symbol2.svg.png",
            "url": "http://www.emmausvienne.fr/article-lyon/74-comment-donner-a-emmaus",
            "service_type_id": 2,
            "obj_type": [
                0
            ]
        },
        {
            "name": "SRPM SAV",
            "description": "Description de l'associaiton / entreprise ...",
            "logo": "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a3/Recycling_symbol2.svg/1024px-Recycling_symbol2.svg.png",
            "url": "http://www.srpm-sav.com/",
            "service_type_id": 1,
            "obj_type": [
                0
            ]
        },
        {
            "name": "SEM Boutique",
            "description": "Description de l'associaiton / entreprise ...",
            "logo": "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a3/Recycling_symbol2.svg/1024px-Recycling_symbol2.svg.png",
            "url": "https://www.semboutique.com/6-pieces-electromenager-lyon",
            "service_type_id": 1,
            "obj_type": [
                0
            ]
        },
        {
            "name": "Docteur IT",
            "description": "Description de l'associaiton / entreprise ...",
            "logo": "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a3/Recycling_symbol2.svg/1024px-Recycling_symbol2.svg.png",
            "url": "http://lyon.docteur-it.com/",
            "service_type_id": 0,
            "obj_type": [
                1
            ]
        },
        {
            "name": "Domphone69",
            "description": "Description de l'associaiton / entreprise ...",
            "logo": "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a3/Recycling_symbol2.svg/1024px-Recycling_symbol2.svg.png",
            "url": "https://www.domphone69.fr/",
            "service_type_id": 1,
            "obj_type": [
                1
            ]
        },
        {
            "name": "La clinique du smartphone",
            "description": "Description de l'associaiton / entreprise ...",
            "logo": "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a3/Recycling_symbol2.svg/1024px-Recycling_symbol2.svg.png",
            "url": "https://www.lacliniquedusmartphone.com/",
            "service_type_id": 1,
            "obj_type": [
                1
            ]
        },
        {
            "name": "PC Réparation",
            "description": "Description de l'associaiton / entreprise ...",
            "logo": "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a3/Recycling_symbol2.svg/1024px-Recycling_symbol2.svg.png",
            "url": "http://www.pc-reparation.fr/",
            "service_type_id": 1,
            "obj_type": [
                1
            ]
        },
        {
            "name": "Points de collecte Grand Lyon",
            "description": "Description de l'associaiton / entreprise ...",
            "logo": "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a3/Recycling_symbol2.svg/1024px-Recycling_symbol2.svg.png",
            "url": "http://www.pc-reparation.fr/",
            "service_type_id": 2,
            "obj_type": [
                1
            ]
        },
        {
            "name": "La clinique de la tablette",
            "description": "Description de l'associaiton / entreprise ...",
            "logo": "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a3/Recycling_symbol2.svg/1024px-Recycling_symbol2.svg.png",
            "url": "http://www.lacliniquedelatablette.fr/",
            "service_type_id": 1,
            "obj_type": [
                1
            ]
        },
        {
            "name": "Le bazar du portable",
            "description": "Description de l'associaiton / entreprise ...",
            "logo": "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a3/Recycling_symbol2.svg/1024px-Recycling_symbol2.svg.png",
            "url": "https://www.bazarduportable.com/",
            "service_type_id": 1,
            "obj_type": [
                1
            ]
        },
        {
            "name": "Le bazar du portable",
            "description": "Description de l'associaiton / entreprise ...",
            "logo": "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a3/Recycling_symbol2.svg/1024px-Recycling_symbol2.svg.png",
            "url": "https://www.bazarduportable.com/",
            "service_type_id": 1,
            "obj_type": [
                1
            ]
        },
        {
            "name": "Rénovation cuir Lyon",
            "description": "Description de l'associaiton / entreprise ...",
            "logo": "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a3/Recycling_symbol2.svg/1024px-Recycling_symbol2.svg.png",
            "url": "https://www.renovation-cuir-lyon.fr/",
            "service_type_id": 0,
            "obj_type": [
                2
            ]
        },
        {
            "name": "Supermano",
            "description": "Description de l'associaiton / entreprise ...",
            "logo": "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a3/Recycling_symbol2.svg/1024px-Recycling_symbol2.svg.png",
            "url": "https://www.supermano.fr/ville-lyon-69000/reparer-meuble-142",
            "service_type_id": 0,
            "obj_type": [
                2
            ]
        },
        {
            "name": "Electricien service",
            "description": "Description de l'associaiton / entreprise ...",
            "logo": "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a3/Recycling_symbol2.svg/1024px-Recycling_symbol2.svg.png",
            "url": "https://www.electricien-service.fr/reparation-luminaire-lyon-69001/",
            "service_type_id": 0,
            "obj_type": [
                3
            ]
        },
        {
            "name": "Atelier Aldo Peaucelle",
            "description": "Description de l'associaiton / entreprise ...",
            "logo": "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a3/Recycling_symbol2.svg/1024px-Recycling_symbol2.svg.png",
            "url": "http://www.atelier-aldo-peaucelle.com/",
            "service_type_id": 0,
            "obj_type": [
                3
            ]
        },
        {
            "name": "Verrerie d'art",
            "description": "Description de l'associaiton / entreprise ...",
            "logo": "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a3/Recycling_symbol2.svg/1024px-Recycling_symbol2.svg.png",
            "url": "https://www.verreriedartdamboise.com/",
            "service_type_id": 0,
            "obj_type": [
                3
            ]
        },
        {
            "name": "Donnons",
            "description": "Description de l'associaiton / entreprise ...",
            "logo": "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a3/Recycling_symbol2.svg/1024px-Recycling_symbol2.svg.png",
            "url": "https://donnons.org/annonces/Lyon-69?distance=0",
            "service_type_id": 2,
            "obj_type": [
                0,
                1,
                2,
                3,
                4,
                5
            ]
        },
        {
            "name": "Tout donner",
            "description": "Description de l'associaiton / entreprise ...",
            "logo": "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a3/Recycling_symbol2.svg/1024px-Recycling_symbol2.svg.png",
            "url": "https://www.toutdonner.com",
            "service_type_id": 2,
            "obj_type": [
                0,
                1,
                2,
                3,
                4,
                5
            ]
        },
        {
            "name": "Bricorama",
            "description": "Description de l'associaiton / entreprise ...",
            "logo": "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a3/Recycling_symbol2.svg/1024px-Recycling_symbol2.svg.png",
            "url": "https://www.bricorama.fr/magasins/bricorama-lyon.html",
            "service_type_id": 1,
            "obj_type": [
                2,
                3
            ]
        },
        {
            "name": "CAAA",
            "description": "Description de l'associaiton / entreprise ...",
            "logo": "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a3/Recycling_symbol2.svg/1024px-Recycling_symbol2.svg.png",
            "url": "https://www.caaa.fr/",
            "service_type_id": 0,
            "obj_type": [
                4
            ]
        },
        {
            "name": "Decathlon",
            "description": "Description de l'associaiton / entreprise ...",
            "logo": "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a3/Recycling_symbol2.svg/1024px-Recycling_symbol2.svg.png",
            "url": "https://www.decathlon.fr/",
            "service_type_id": 0,
            "obj_type": [
                4
            ]
        },
        {
            "name": "Decathlon",
            "description": "Description de l'associaiton / entreprise ...",
            "logo": "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a3/Recycling_symbol2.svg/1024px-Recycling_symbol2.svg.png",
            "url": "https://www.decathlon.fr/",
            "service_type_id": 0,
            "obj_type": [
                4
            ]
        },
        {
            "name": "Decathlon",
            "description": "Description de l'associaiton / entreprise ...",
            "logo": "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a3/Recycling_symbol2.svg/1024px-Recycling_symbol2.svg.png",
            "url": "https://www.decathlon.fr/",
            "service_type_id": 1,
            "obj_type": [
                4
            ]
        },
        {
            "name": "WallStreet Skateshop",
            "description": "Description de l'associaiton / entreprise ...",
            "logo": "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a3/Recycling_symbol2.svg/1024px-Recycling_symbol2.svg.png",
            "url": "https://wallstreetskateshop.fr/",
            "service_type_id": 0,
            "obj_type": [
                4
            ]
        },
        {
            "name": "WallStreet Skateshop",
            "description": "Description de l'associaiton / entreprise ...",
            "logo": "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a3/Recycling_symbol2.svg/1024px-Recycling_symbol2.svg.png",
            "url": "https://wallstreetskateshop.fr/",
            "service_type_id": 1,
            "obj_type": [
                4
            ]
        },
        {
            "name": "WallStreet Skateshop",
            "description": "Description de l'associaiton / entreprise ...",
            "logo": "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a3/Recycling_symbol2.svg/1024px-Recycling_symbol2.svg.png",
            "url": "https://wallstreetskateshop.fr/",
            "service_type_id": 2,
            "obj_type": [
                4
            ]
        },
        {
            "name": "Nagabbo",
            "description": "Description de l'associaiton / entreprise ...",
            "logo": "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a3/Recycling_symbol2.svg/1024px-Recycling_symbol2.svg.png",
            "url": "https://www.nagabbo-opticiens.com/content/19-services-nagabbo",
            "service_type_id": 0,
            "obj_type": [
                5
            ]
        },
        {
            "name": "Nagabbo",
            "description": "Description de l'associaiton / entreprise ...",
            "logo": "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a3/Recycling_symbol2.svg/1024px-Recycling_symbol2.svg.png",
            "url": "https://www.nagabbo-opticiens.com/content/19-services-nagabbo",
            "service_type_id": 1,
            "obj_type": [
                5
            ]
        }
    ]
};
